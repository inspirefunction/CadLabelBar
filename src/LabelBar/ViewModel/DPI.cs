﻿namespace JoinBox.LabelBar.ViewModel;

using System;
using System.Drawing;

// 同高版本的Autodesk.AutoCAD.Windows.Window.State
public enum State
{
    Normal = 0,    // 无状态
    Minimized = 1, // 最小化
    Maximized = 2  // 最大化
}

public class DPI
{
    public static double CurrentDPI => Graphics.FromHwnd(IntPtr.Zero).DpiX / 96;
}