﻿namespace JoinBox.LabelBar.ViewModel;

using System.Collections.ObjectModel;
using JoinBox.WPF;

#if !cad
// 测试替代
public class Document
{
    public Database Database { get; }
    public string Name { get; }

    public Document()
    {
        Database = new();
        Name = Database.Filename;
    }
}
public class Database
{
    public string Filename { get; }
    public Database()
    {
        Filename = "Filename";
    }
}
#endif

/// <summary>
/// 限制所有高度的初始化值
/// </summary>
public class DocumentTab
{
    public static int Height { set; get; } = 30;
    public static int HeightDPI => (int)(Height * DPI.CurrentDPI);
}

public class DocElement : ViewModelBase
{
    /// <summary>
    /// 文档栏上面显示的文档名称
    /// </summary>
    public string InfoDocName { get; set; }

    Document? _Document;
    public Document? Document
    {
        get { return _Document; }
        set { _Document = value; OnPropertyChanged(); }
    }

    bool _pick;
    /// <summary>
    /// 选中
    /// </summary>
    public bool Pick
    {
        get { return _pick; }
        set { _pick = value; OnPropertyChanged(); }
    }

    public DocElement(string infoDocName)
    {
        InfoDocName = infoDocName;
    }
}

/// <summary>
/// WPF联动数据
/// </summary>
public partial class DocumentTabData : ViewModelBase
{
    #region 字段
    int _height;
    /// <summary>
    /// 多文档标签窗口高度
    /// </summary>
    public int DocWindowHeight
    {
        get => _height;
        set => SetProperty(ref _height, value);
    }

    ObservableCollection<DocElement> _DocElements;
    /// <summary>
    /// 多文档标签集合
    /// </summary>
    public ObservableCollection<DocElement> DocElements
    {
        get => _DocElements;
        set => SetProperty(ref _DocElements, value);
    }

    DocElement _DocElementPick;
    /// <summary>
    /// 用于控制文档按钮选中
    /// </summary>
    public DocElement DocElementPick
    {
        get => _DocElementPick;
        set { _DocElementPick = value; OnPropertyChanged(); }
    }

    Document? _intput;
    /// <summary>
    /// 按钮选中状态
    /// </summary>
    public Document? InputPick
    {
        get { return _intput; }
        set
        {
            _intput = value;
            OnPropertyChanged();

            DocElement? pickitem = null;
            foreach (DocElement it in DocElements)
                if (it.Document == _intput)
                    pickitem = it;
                else
                    it.Pick = false;

            // 含有字母,就选中
            if (pickitem != null)
                pickitem.Pick = true;
        }
    }
    #endregion

    // 数据双向关联
    // 0x01 数据绑定到xaml的cs文件 DataContext = new PipeViewModel();
    // 0x02 资源文件当前找不到,会找上面一层,再找不到,再找再上一层,直到最后根目录.
    public DocumentTabData()
    {
        DocWindowHeight = DocumentTab.Height;
        _DocElements = new();
        _DocElementPick = new(string.Empty);
    }
}