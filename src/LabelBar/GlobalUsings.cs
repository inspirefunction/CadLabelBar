﻿/// 系统引用
global using System;
global using System.Collections;
global using System.Collections.Generic;
global using System.IO;
global using System.Linq;
global using System.Text;
global using System.Reflection;
global using System.Text.RegularExpressions;
global using Microsoft.Win32;
global using System.ComponentModel;
global using System.Runtime.InteropServices;
global using System.Collections.Specialized;

global using Exception = System.Exception;

global using Registry = Microsoft.Win32.Registry;
global using RegistryKey = Microsoft.Win32.RegistryKey;


global using System.Net.Sockets;
global using System.Net;
global using System.Management;  // 在项目-》添加引用....里面引用System.Management
global using System.Diagnostics;
global using System.Threading;
global using System.Runtime.Serialization.Formatters.Binary;
global using Formatting = Newtonsoft.Json.Formatting;
global using System.Data.OleDb;// 数据库
global using Microsoft.CSharp;// 动态编译
global using System.Xml;
global using System.Xml.Serialization;
global using System.Xml.Linq;
global using Newtonsoft.Json;

// WPF
global using GalaSoft.MvvmLight;
global using System.Windows;
global using System.Windows.Interop;
global using System.Windows.Controls;
global using System.Windows.Input;
global using System.Windows.Forms;
global using System.Collections.ObjectModel;
global using Point = System.Drawing.Point;

#if acad
/// cad 引用
global using Autodesk.AutoCAD.ApplicationServices;
global using Autodesk.AutoCAD.EditorInput;
global using Autodesk.AutoCAD.Colors;
global using Autodesk.AutoCAD.DatabaseServices;
global using Autodesk.AutoCAD.Geometry;
global using Autodesk.AutoCAD.Runtime;
global using Acap = Autodesk.AutoCAD.ApplicationServices.Application;
global using Acgi = Autodesk.AutoCAD.GraphicsInterface;

global using Autodesk.AutoCAD.DatabaseServices.Filters;
global using Autodesk.AutoCAD;
global using ErrorStatus = Autodesk.AutoCAD.Runtime.ErrorStatus;

/// jig命名空间会引起Viewport/Polyline等等重义,最好逐个引入 using Autodesk.AutoCAD.GraphicsInterface
global using Autodesk.AutoCAD.GraphicsInterface;
global using WorldDraw = Autodesk.AutoCAD.GraphicsInterface.WorldDraw;
global using Manager = Autodesk.AutoCAD.GraphicsSystem.Manager;
global using Group = Autodesk.AutoCAD.DatabaseServices.Group;
global using Viewport = Autodesk.AutoCAD.DatabaseServices.Viewport;
global using Polyline = Autodesk.AutoCAD.DatabaseServices.Polyline;
global using Cad_DwgFiler = Autodesk.AutoCAD.DatabaseServices.DwgFiler;
global using Cad_DxfFiler = Autodesk.AutoCAD.DatabaseServices.DxfFiler;
global using Cad_ErrorStatus = Autodesk.AutoCAD.Runtime.ErrorStatus;
global using CursorType = Autodesk.AutoCAD.EditorInput.CursorType;
global using Tolerance = Autodesk.AutoCAD.Geometry.Tolerance;

/// cad文档栏项目需要
global using SystemVariableChangedEventArgs = Autodesk.AutoCAD.ApplicationServices.SystemVariableChangedEventArgs;
global using SystemVariableChangingEventArgs = Autodesk.AutoCAD.ApplicationServices.SystemVariableChangingEventArgs;
global using static Autodesk.AutoCAD.Windows.SaveFileDialog;
global using static Autodesk.AutoCAD.Windows.OpenFileDialog;
/// cad.com接口
#if acadcom
global using Autodesk.AutoCAD.Interop;
global using Autodesk.AutoCAD.Interop.Common;
global using ToolbarDockStatus = Autodesk.AutoCAD.Interop.Common.AcToolbarDockStatus;
#endif
/// AcCui.dll引用
global using Autodesk.AutoCAD.Customization;
global using Autodesk.AutoCAD.Windows;
global using MenuItem = Autodesk.AutoCAD.Windows.MenuItem;

/// 打印机
global using Autodesk.AutoCAD.PlottingServices;
global using Autodesk.AutoCAD.LayerManager;
global using PlotType = Autodesk.AutoCAD.DatabaseServices.PlotType;
global using LayerFilter = Autodesk.AutoCAD.LayerManager.LayerFilter;
global using AttributeCollection = Autodesk.AutoCAD.DatabaseServices.AttributeCollection;
global using Vertex = Autodesk.AutoCAD.DatabaseServices.Vertex;

#if !NET35
global using Autodesk.AutoCAD.BoundaryRepresentation;
global using BR_Face = Autodesk.AutoCAD.BoundaryRepresentation.Face;
global using BR_Edge = Autodesk.AutoCAD.BoundaryRepresentation.Edge;
#endif
#endif

/// 惊惊盒子
global using LoopState = JoinBox.Basal.LoopState;
global using JoinBox.Basal;
global using JoinBox.Forms;
global using JoinBox.WPF;
global using ViewModelBase = JoinBox.WPF.ViewModelBase;
global using static JoinBox.Basal.WindowsAPI;
global using WindowsAPI = JoinBox.Basal.WindowsAPI;

#if JoinBox_Math
// 这里需要加入不安全代码
global using JoinBox.MathEx;
global using Rect = JoinBox.MathEx.Rect;
#endif

#if cad
global using JoinBox.Cad;

global using IFoxCAD.Basal;
global using IFoxCAD.Cad;
global using IFoxCAD.LoadEx;
#endif