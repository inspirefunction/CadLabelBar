﻿#if true2
namespace JoinBox.LabelBar.View;

public class SaveAsClass
{
    [CommandMethod(nameof(CadCmdSaveAs), CommandFlags.Session)]
    public void CadCmdSaveAs()
    {
        PostSaveAs(Acap.DocumentManager.MdiActiveDocument);
    }

    public void SaveAs(DocElement info)
    {
        PostSaveAs(info.Document);
    }

    /// <summary>
    /// 另存为_dwg<br/>
    /// 此方法已被 <see cref="DocWindow.Post_SaveAs"/>代替
    /// </summary>
    public void PostSaveAs(Document? doc)
    {
        if (doc == null)
            return;

        string? dwgNewFilename = null;
        var cp = new AcadRegeditCproFile();
        var lastSavedPath = cp.Places[0].PathValue;

        // 另存为的标题:涉及了历史文件夹上面的文件夹名称,所以是不能改的
        // 历史文件夹位置: C:\Users\LQH\AppData\Roaming\Autodesk\AutoCAD 2008\R17.1\chs\Recent
        var lincunwei = "图形另存为";
        if (!Directory.Exists(lastSavedPath))
        {
            // cad的另存为最初设置的路径不知道如何设置的.
            // 它会以一个dll加载过的路径?
            // 但是我另存过之后,去保存到注册表上的路径,和"cad另存为"取到的路径是一样的.
            if (lastSavedPath == "History")// 历史
            {
                // 从打印路径中截取这个另存为的路径,
                // 不太明白cad为什么没有设置变量--或许是我不知道呢~
                var pr = Env.GetEnv("PrinterConfigDir");
                pr = pr.Replace("plotters", null);
                lastSavedPath = pr + "Recent\\" + lincunwei;
                if (!Directory.Exists(lastSavedPath))
                    Directory.CreateDirectory(lastSavedPath);
            }
            else
            {
                bool isPath = true;
                foreach (var item in Enum.GetValues(typeof(Environment.SpecialFolder)))
                {
                    if (item.ToString() == lastSavedPath)  // 获取枚举的每项的名称
                    {
                        var itemv = (Environment.SpecialFolder)item;
                        lastSavedPath = Environment.GetFolderPath(itemv);
                        isPath = false;
                        break;
                    }
                }
                if (isPath)
                    lastSavedPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            }
        }

        var saveDwgName = DocWindow.GetDocName(doc);
        var sa = new Autodesk.AutoCAD.Windows.SaveFileDialog(
        lincunwei, lastSavedPath + "\\" + saveDwgName, "dwg", lincunwei,
        SaveFileDialogFlags.AllowAnyExtension | SaveFileDialogFlags.ForceDefaultFolder);

        var okno = sa.ShowDialog();

        if (okno == System.Windows.Forms.DialogResult.OK)
        {
            // 储存回注册表
            lastSavedPath = Path.GetDirectoryName(sa.Filename);
            cp.Save(lastSavedPath);

            // 非当前文档数据库另存为的时候发现一件很奇怪的事情,
            // 另存为是成功的,但是另存的dwg不会被当前cad占用
            // 要怎么产生占用呢?

            // 这里怎么拿cad的用户配置格式呢?
            // cad08格式
            var dwgVersion = DwgVersion.Current;
            if ((int)dwgVersion > 27)// 大于的话用08格式,小于等于都使用当前
                dwgVersion = (DwgVersion)27;
            doc.Database.SaveAs(sa.Filename, dwgVersion);

            // 另存为的时候判断如果不是当前活动图纸,
            // 就发送保存,利用保存的来实现另存,从而占用文档
            // if (true)
            //    Qsave(info);

            dwgNewFilename = sa.Filename;
        }

        if (dwgNewFilename == null)
            return;

        // 另存为的时候可能修改文件名,所以不能用命令的方式,有了,替换掉用户的另存为命令
        // 命令执行的时候是cad的线程,需要跨线程
        if (CadCommand.Anchor?.WPFDocWindow == null)
            return;
        AutoGo.Post(() => {
            foreach (var info in CadCommand.Anchor.WPFDocWindow.TabData.DocElements)
            {
                if (info.Document == doc)
                {
                    info.InfoDocName = DocWindow.GetDocName(doc);
                    info.Document = doc;// 实现WPF更新
                }
            }
        });
    }
}
#endif