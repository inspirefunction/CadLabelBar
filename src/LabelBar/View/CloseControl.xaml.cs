﻿namespace JoinBox.LabelBar.View;

using System.Windows.Controls;
using System.Windows;
using System.Windows.Input;

/// <summary>
/// CloseControl.xaml 的交互逻辑
/// </summary>
public partial class CloseControl : UserControl
{
    // 添加按钮绑定依赖属性-绑定命令用
    public ICommand ActionCommand
    {
        get { return (ICommand)GetValue(ActionCommandProperty); }
        set { SetValue(ActionCommandProperty, value); }
    }

    // 使用 DependencyProperty 作为 ActionCommand 的后备存储。这使动画,样式,绑定等。
    public static readonly DependencyProperty ActionCommandProperty =
        DependencyProperty.Register(nameof(ActionCommand), typeof(ICommand), typeof(CloseControl), new PropertyMetadata(null));


    // 添加按钮绑定依赖属性-绑定命令参数用
    public object CommandParameter
    {
        get { return GetValue(CommandParameterProperty); }
        set { SetValue(CommandParameterProperty, value); }
    }

    public static readonly DependencyProperty CommandParameterProperty =
        DependencyProperty.Register(nameof(CommandParameter), typeof(object), typeof(CloseControl), new PropertyMetadata(null));


    public string CloseText { get; set; }

    public CloseControl()
    {
        CloseText = "";
        InitializeComponent();
        this.DataContext = this;
    }
}