﻿#define AutoLoad
namespace JoinBox.LabelBar;
using JoinBox.LabelBar.View;

#if AutoLoad
public class CadLabelBarInit : IFoxCAD.Cad.AutoRegAssem
{
    // 加载dll就启动多文档标签,且可以通过命令反复调用
    public CadLabelBarInit() : base(AutoRegConfig.All)
    {
        CadCommand.CadLabelBarInit = this;
        CadCommand.AddDou();
    }
    [IFoxInitialize(isInitialize: false)]
    public static new void Terminate()
    {
        CadCommand.RemoveAnchor();
    }
}
#else
public class CadLabelBarInit : IExtensionApplication
{
    public void Initialize()
    {
        CadCommand.CadLabelBarInit = this;
        CadCommand.AddDou();
    }
    public void Terminate()
    {
        CadCommand.RemoveAnchor();
    }
}
#endif

// 命令不可以放到 Init 否则用卸载命令的时候会重入构造函数
public class CadCommand
{
    public static CadLabelBarInit? CadLabelBarInit;// 装入以免释放,变成一个交叉类
    public static Anchor? Anchor;

    // 用命令来移除多文档标签
    [CommandMethod("+Duo")]
    public static void CmdAddDou()
    {
        var ed = Acap.DocumentManager.MdiActiveDocument.Editor;
        ed.WriteMessage("\n添加多文档标签");
        AddDou();
    }

    public static void AddDou()
    {
        // 利用初始化添加注册表
        CadLabelBarInit ??= new();
        RemoveAnchor();
        Anchor = new();
    }

    // 用命令来移除多文档标签
    [CommandMethod("-Duo")]
    public static void CmdRemoveDou()
    {
        // regedit
        // HKEY_CURRENT_USER\Software\Autodesk\AutoCAD\R24.0\ACAD-4101:804\Applications
        var ed = Acap.DocumentManager.MdiActiveDocument.Editor;
        if (Anchor == null)
            ed.WriteMessage("\n移除多文档标签,并卸载了注册表:它已经卸载了哦~");
        else
            ed.WriteMessage("\n移除多文档标签,并卸载了注册表");

        RemoveAnchor();
        RemoveRegApp();
    }

    public static void RemoveAnchor()
    {
        if (Anchor == null || Anchor.IsDisposed)
        {
            Anchor = null;
            return;
        }
        Anchor.Dispose();
        Anchor = null;
    }

    public static void RemoveRegApp()
    {
#if AutoLoad
        CadLabelBarInit?.UnRegApp();
#endif
        CadLabelBarInit = null;
    }

    [CommandMethod(nameof(QnewMethod), CommandFlags.Modal | CommandFlags.Session)]
    public static void QnewMethod()
    {
        Debugx.Printl(nameof(QnewMethod));

        // 如何重置才使用这个修复注册表呢?
        // AcadRegistry.RepairRegistry();

        var lastTemplate = Env.GetEnv("LastTemplate");// 最后使用的模板
        var templatePath = Env.GetEnv("TemplatePath");// 模板路径
        if (!File.Exists(lastTemplate))
            lastTemplate = templatePath + "\\acad.dwt";
        const string title = "惊!选择模板";
        string openName;

#if FormsOpenDialog
        var dialog = new System.Windows.Forms.OpenFileDialog
        {
            Title = title,
            Filter = "模板文件(*.dwt)|*.dwt|所有文件(*.*)|*.*",
            FileName = "acad.dwt",
            InitialDirectory = templatePath,// 设置打开路径的目录
        };
        if (dialog.ShowDialog() != System.Windows.Forms.DialogResult.OK)
            return;
        openName = dialog.FileName;
#else
        var dialog = new Autodesk.AutoCAD.Windows.OpenFileDialog(
                         title, lastTemplate, "dwt", "",
                         OpenFileDialogFlags.AllowAnyExtension
                         | OpenFileDialogFlags.ForceDefaultFolder);

        if (dialog.ShowDialog() != DialogResult.OK)
            return;
        openName = dialog.Filename;
#endif
        // System.Runtime.InteropServices.COMException:“内部应用程序出错。”
        // 如果这里出现这个错误,是因为重置引起,
        // 重置就会把注册表(和配置文件)删掉,复现这个错误.
        // 而 Acap.DocumentManager.Add 需要其中的注册表,才不会报错
        // 卡死因为没有 CommandFlags.Session

        Acap.DocumentManager.Add(openName);
        // 之后会进入激活文档事件 DocumentManager_DocumentActivated
    }
}