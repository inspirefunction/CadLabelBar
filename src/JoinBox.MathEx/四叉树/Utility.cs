﻿namespace JoinBox.MathEx;

using System;
using System.Text;
using Color = System.Drawing.Color;

public static class Utility
{
    public static Random GetRandom()
    {
        var tick = DateTime.Now.Ticks;
        var tickSeeds = (int)(tick & 0xffffffffL) | (int)(tick >> 32);
        return new Random(tickSeeds);
    }

    /// <summary>
    /// 随机颜色
    /// </summary>
    /// <returns></returns>
    public static Color RandomColor
    {
        get
        {
            var ran = GetRandom();
            int R = ran.Next(255);
            int G = ran.Next(255);
            int B = ran.Next(255);
            B = (R + G > 400) ? R + G - 400 : B;// 0 : 380 - R - G;
            B = (B > 255) ? 255 : B;
            return Color.FromArgb(R, G, B);
        }
    }

    /// <summary>
    /// 生成制定位数的随机码（数字）
    /// </summary>
    /// <param name="length">随机长度</param>
    /// <returns></returns>
    public static string GenerateRandomCode(int length)
    {
        var result = new StringBuilder();
        for (var i = 0; i < length; i++)
        {
            var r = new Random(Guid.NewGuid().GetHashCode());
            result.Append(r.Next(0, 10));
        }
        return result.ToString();
    }
}