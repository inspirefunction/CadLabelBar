﻿namespace JoinBox.MathEx.Algorithm;

/// <summary>
/// 染色体
/// </summary>
public class Chromosome
{
    /// <summary>
    /// 基因编码:用6bit对染色体进行编码
    /// </summary>
    public int[] Bits = new int[6];

    /// <summary>
    /// 适应度
    /// </summary>
    public int Fitness;

    /// <summary>
    /// 适应度百分比(选择概率)
    /// </summary>
    public double FitPercent;

    /// <summary>
    /// 累积概率
    /// </summary>
    public double Probability;

    /// <summary>
    /// 染色体
    /// </summary>
    public Chromosome() { }

    /// <summary>
    /// 克隆
    /// </summary>
    /// <returns></returns>
    public Chromosome Clone()
    {
        var c = new Chromosome();
        for (int i = 0; i < Bits.Length; i++)
            c.Bits[i] = Bits[i];

        c.Fitness = Fitness;
        c.FitPercent = FitPercent;
        c.Probability = Probability;
        return c;
    }
}