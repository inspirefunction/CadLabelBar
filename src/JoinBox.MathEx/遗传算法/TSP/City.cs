﻿namespace JoinBox.MathEx.TSP;

using JoinBox.MathEx;
using System;

public class City
{
    /// <summary>
    /// 城市坐标
    /// </summary>
    public PointV Point { get; private set; }

    /// <summary>
    /// 城市名称
    /// </summary>
    public string? CityName;

    /// <summary>
    /// 城市
    /// </summary>
    public City(double x, double y, string? cityName = null)
    {
        Point    = new PointV(x, y);
        CityName = cityName;
    }
    public City(PointV point, string? cityName = null)
    {
        Point    = point;
        CityName = cityName;
    }

    /// <summary>
    /// 距离
    /// </summary>
    /// <param name="c"></param>
    /// <returns></returns>
    public double DistanceTo(City c)
    {
        return Point.GetDistanceTo(c.Point);
    }

    /// <summary>
    /// 生成随机点的城市
    /// </summary>
    /// <param name="random"></param>
    /// <returns></returns>
    public static City Random(Random random)
    {
        return new City(random.NextDouble(), random.NextDouble());
    }
}