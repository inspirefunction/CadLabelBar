﻿namespace JoinBox.MathEx.TSP;

using System;


public enum SelectMode
{
    /// <summary>
    /// 轮盘赌
    /// </summary>
    Roulette,
    /// <summary>
    /// 锦标赛
    /// </summary>
    Tournament,
    /// <summary>
    /// 冒泡排序
    /// </summary>
    SortBubble,
}

/// <summary>
/// 参数类
/// </summary>
public class GeneticInfoBasal
{
#pragma warning disable CA2211 // 非常量字段应当不可见
    /// <summary>
    /// 迭代次数
    /// </summary>
    public int Iterations = -1;

    /// <summary>
    /// 染色体数量(保留精英)
    /// </summary>
    public int ChromosomeNumber = 20;

    /// <summary>
    /// 变异概率
    /// </summary>
    public double MutateProbability = 0.04;

    /// <summary>
    /// 无响应时,变异概率叠加的时间,-1是死循环(更好求出最优解),2000(大概一分钟就结束)
    /// </summary>
    public int EvolveTime = 2_000;

    /// <summary>
    /// 选择模式
    /// </summary>
    public SelectMode SelectMode = SelectMode.Roulette;
    static readonly Random random = new();
    public static Random Random = random;

#pragma warning restore CA2211 // 非常量字段应当不可见
}

public interface ISelectModeMethod
{
    /// <summary>
    /// 选择染色体方法:排序
    /// </summary>
    void SelectSortBubble();

    /// <summary>
    /// 选择染色体方法:轮盘赌,进行基因型的选择,更新下一代
    /// </summary>
    void SelectRoulette();

    /// <summary>
    /// 选择染色体方法:锦标赛
    /// </summary>
    void SelectTournament();
}

public interface IGeneticMethod
{
    /// <summary>
    /// 交叉(交配生子)
    /// </summary>
    void Crossover();

    /// <summary>
    /// 变异
    /// </summary>
    void Mutate();
}
/*
public interface GeneticMethod<T> where T : new()
{
    /// <summary>
    /// 交叉(交配生子)
    /// </summary>
    /// <param name="a">男</param>
    /// <param name="b">女</param>
    /// <returns>孩子可多个</returns>
    T[] Crossover(T a, T b);

    /// <summary>
    /// 变异
    /// </summary>
    /// <param name="a">原有</param>
    /// <returns>变异后的</returns>
    T Mutate(T a);
}
*/
