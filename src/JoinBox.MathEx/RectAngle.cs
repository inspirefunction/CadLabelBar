﻿namespace JoinBox.MathEx;


using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;


[Serializable]
[StructLayout(LayoutKind.Sequential)]
internal class RectAngle : Rect
{
    /// <summary>
    /// 角度(此矩形绕点旋转是通过左下min点的)
    /// </summary>
    public double Angle;

#if cad
    public RectAngle(PointV point1, PointV point2, PointV point3, PointV point4) :
        this(new List<PointV>() { point1, point2, point3, point4 })
    {
    }

    public RectAngle(List<PointV> pts)
    {
        bool isRect = Rect.RectAnglePointOrder(pts);
        if (!isRect)
            throw new ArgumentException(nameof(RectAngle) + "不是矩形!");

        var min = pts[0];
        Angle = min.GetVectorTo(pts[1]).GetAngle2XAxis();
        _X = min.X;
        _Y = min.Y;

        var pt3 = pts[3].RotateBy(-Angle, PointV.ZAxis, min);
        _Right = pt3.X;
        _Top = pt3.Y;
    }
#endif

    /// <summary>
    /// 矩形对角
    /// </summary>
    /// <param name="pt1"></param>
    /// <param name="pt3"></param>
    public RectAngle(PointV pt1, PointV pt3) : base(pt1, pt3, true)
    {
        Angle = 0;
    }
    public RectAngle(Rect rect, double angle)
    {
        Angle = angle;
        _X = rect._X;
        _Y = rect._Y;
        _Right = rect._Right;
        _Top = rect._Top;
    }
}