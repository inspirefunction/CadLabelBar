﻿namespace WPF_Evn;

using System;
using System.Windows;
using System.Windows.Interop;

/// <summary>
/// MainWindow.xaml 的交互逻辑
/// </summary>
public partial class MainWindow : Window
{
    public MainWindow()
    {
        InitializeComponent();
    }

    #region 嵌入设定
    public IntPtr Handel => new WindowInteropHelper(this).Handle;// WPF句柄

    IntPtr _Owner;
    /// <summary>
    /// 通过指针设置所有者
    /// 使得使用枚举启动位置时候展示生效
    /// <see cref="System.Windows.WindowStartupLocation"/>
    /// </summary>
    public new IntPtr Owner
    {
        get => _Owner;
        set
        {
            new WindowInteropHelper(this)
            {
                Owner = value// 设置所有者
            };
            _Owner = Owner;
        }
    }

    /// <summary>
    /// 设置最大化,免得嵌入之后可以选中
    /// </summary>
    public void SizeSet()
    {
        // 先设置一次Normal使它能从新计算和容器的大小关系
        if (this.WindowState == WindowState.Maximized)
            this.WindowState = WindowState.Normal;
        this.WindowState = WindowState.Maximized;
    }
    #endregion

}
