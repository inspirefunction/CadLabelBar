﻿namespace JoinBox.Cad;

using System;
using System.Windows.Controls;

public static class AutoGo
{
    /// <summary>
    /// Acap上下文_文档窗口管理器
    /// </summary>
    public static SyncEx AcapMidSyncEx
    {
        get
        {
            if (_AcapMidSyncContext == null)
            {
                var dm = Acap.DocumentManager;
                var doc = dm.MdiActiveDocument;
                if (doc == null)
                    throw new ArgumentNullException("无法处理的错误::" + nameof(AcapMidSyncEx));
                _AcapMidSyncContext = new(WindowsAPI.GetParent(doc.Window.Handle));
            }
            return _AcapMidSyncContext;
        }
    }
    static volatile SyncEx? _AcapMidSyncContext;

    /// <summary>
    /// Acap上下文_主窗口
    /// </summary>
    public static SyncEx AcapMainSyncEx
    {
        get
        {
            _AcapMainSyncEx ??= new(Acap.MainWindow.Handle);
            return _AcapMainSyncEx;
        }
    }
    static volatile SyncEx? _AcapMainSyncEx;

    /// <summary>
    /// 在其他线程Post异步发送到cad主线程(Send无法响应鼠标)<br/>
    /// 不要在cad主线程Post否则卡死
    /// </summary>
    /// <param name="action"></param>
    public static void Post(Action action)
    {
        AcapMainSyncEx.Post(action);
    }

    // 此处应该改成消息队列
    public static void Printl(this object str)
    {
        AutoGo.AcapMainAwait(AutoGo.AwaitMode.HasDocument, () => {
            AcapMainSyncEx.Post(() => {
                var dm = Acap.DocumentManager;
                if (dm is null || dm.Count == 0)
                    throw new ArgumentException(nameof(Printl) + "DocumentManager");
                var doc = dm.MdiActiveDocument;
                if (doc is null)
                    throw new ArgumentException(nameof(Printl) + "MdiActiveDocument");
                var ed = doc.Editor;
                ed.WriteMessage(Environment.NewLine + str.ToString());
            });
        });
        Debug.WriteLine(str);
    }

    /// <summary>
    /// 等候再执行
    /// </summary>
    public enum AwaitMode : int
    {
        NotDocument = 0,
        /// <summary>
        /// 有激活的文档命令期间
        /// </summary>
        HasDocument = 1,
        /// <summary>
        /// 不在命令期间
        /// </summary>
        NotCommand = 2,

        HasDocumentAndNotCommand = HasDocument | NotCommand,
    }


    // 关闭了 Ribbon 菜单就可以没有那么多绑定失败的提示,否则选择一个图元都卡一次
    // System.Windows.Data Error: 40 : BindingExpression path error: 'TableStyle'
    // debug的时候发送命令去关闭它...但是不是每个人都喜欢...
    //if (Debugger.IsAttached)
    //{
    //    "_RibbonClose"
    //}

    // 触发: 即使不加载任何插件,随便按键盘
    // 貌似是键盘 按下 会同时弹出两条信息
    // onecore\com\combase\dcomrem\resolver.cxx(2299)\combase.dll!00007FFF0FEC1BED: (caller: 00007FFF0FEC4ACE) ReturnHr(34) tid(980) 80040154 没有注册类
    // 我搜到这个错误来自cad使用QT重写了之后没有处理com问题导致


    /// <summary>
    /// 创建线程进行循环等待
    /// </summary>
    /// <param name="amam">等候再执行</param>
    /// <param name="runTask">所有等候完成之后的执行任务(仍然新线程内)</param>
    /// <param name="awaitTask">需要补充的等候任务;
    /// 它将会在<paramref name="amam"/>之后进行<br/>
    /// 返回<see langword="false"/>就继续等待</param>
    /// <param name="state">线程方式</param>
    /// <returns>返回创建的线程,用于释放</returns>
    /// <exception cref="ArgumentNullException"></exception>
    public static Thread AcapMainAwait(AwaitMode amam,
                                       Action runTask,
                                       Func<bool>? awaitTask = null,
                                       ApartmentState state = ApartmentState.STA/*WPF需要此类型*/)
    {
        if (runTask == null)
            throw new ArgumentNullException(nameof(runTask));

        const int sleep = 100;
        Thread thread = new(() => {
            var breakFlag = false;
            while (true)
            {
                /// 直到 <see cref="IExtensionApplication"/> 完成,此处才会通过
                if (!Acap.IsQuiescent)
                {
                    SyncContext.DebugPrintl("AcapMainAwait::!Acap.IsQuiescent");
                    Thread.Sleep(sleep);
                    continue;
                }

                /// 如果这里一直是null,表示 <see cref="SyncEx.WndProc"/> 没有实现过一次
                /// 那么就需要在构造函数去默认实现一次
                if (!AcapMainSyncEx.ExistContextCache)
                {
                    SyncContext.DebugPrintl("AcapMainAwait::!AcapMainSyncEx.ExistContextCache");
                    AcapMainSyncEx.Refresh();
                    Thread.Sleep(sleep);
                    continue;
                }
                if ((amam & AwaitMode.HasDocument) == AwaitMode.HasDocument)
                {
                    SyncContext.DebugPrintl("AcapMainAwait::HasDocument");

                    AcapMainSyncEx.Send(() => {
                        SyncContext.DebugPrintl("AcapMainAwait::Send 开始");

                        var dm = Acap.DocumentManager;
                        if (dm is null || dm.Count == 0)
                        {
                            SyncContext.DebugPrintl("AcapMainAwait::Send dm是空的");
                            Thread.Sleep(0);
                            return;
                        }
                        SyncContext.DebugPrintl("AcapMainAwait::Send dm存在");

                        // 初始化时此句导致: 偶发性卡死
                        // 因为此句需要不阻塞主线程,获取属性的时候主线程要调度上下文,
                        // Send线程 会卡在这里等候,
                        // 在过程中,其他 Send线程 也会进入此处跟着等,
                        // 直到某个线程等到了,其他线程就会继续通行.
                        var doc = dm.MdiActiveDocument;
                        if (doc is null)
                        {
                            SyncContext.DebugPrintl("AcapMainAwait::Send doc是空的");
                            Thread.Sleep(0);
                            return;
                        }
                        SyncContext.DebugPrintl("AcapMainAwait::Send doc存在");

                        // 等待命令完成再执行
                        if ((amam & AwaitMode.NotCommand) == AwaitMode.NotCommand)
                        {
                            SyncContext.DebugPrintl("AcapMainAwait::Send 检查是否在cad命令中..");
                            string input = doc.CommandInProgress;
                            if (input != "")
                            {
                                SyncContext.DebugPrintl("AcapMainAwait::Send 在cad命令中");
                                Thread.Sleep(0);
                                return;
                            }
                            SyncContext.DebugPrintl("AcapMainAwait::Send 不在cad命令中");
                        }
                        SyncContext.DebugPrintl("AcapMainAwait::Send 完成");
                        breakFlag = true;
                    });

                    AcapMainSyncEx.Refresh();
                }
                else
                {
                    SyncContext.DebugPrintl("AcapMainAwait::!HasDocument");
                    breakFlag = true;
                }

                if (breakFlag && awaitTask != null)
                    breakFlag = awaitTask.Invoke();
                if (breakFlag)
                {
                    //Debug.WriteLine("AcapMainAwait::break"); //debugx关闭之后,此处看有几个send线程触发
                    SyncContext.DebugPrintl("AcapMainAwait::break");
                    break;
                }
                SyncContext.DebugPrintl("AcapMainAwait::!break");
                Thread.Sleep(sleep);
            }
            runTask.Invoke();
        });
        thread.SetApartmentState(state);
        thread.Start();
        return thread;
    }
}