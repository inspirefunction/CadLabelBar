﻿namespace JoinBox.Forms;

using JoinBox.Basal;
using System;
using System.Diagnostics;

/// <summary>
/// 线程上下文/子类化拦截消息
/// </summary>
public class SyncEx : SyncContext, IDisposable
{
    /// <summary>
    /// 句柄
    /// </summary>
    public IntPtr Handle => _nativeCallProc.Handle;

    /// <summary>
    /// 子类化窗体对象
    /// </summary>
    readonly NativeCallProc _nativeCallProc;

    /// <summary>
    /// 子类化窗体上下文
    /// </summary>
    /// <param name="windowHandle">嵌入到此句柄的窗体</param>
    /// <exception cref="ArgumentException"></exception>
    [DebuggerStepThrough]
    public SyncEx(IntPtr windowHandle)
    {
        if (windowHandle == IntPtr.Zero)
            throw new ArgumentException("子类化失败,句柄为空");

        _nativeCallProc = new(windowHandle);

        /// 如果从来不实现,
        /// 会导致 <see cref="SyncContext.ExistContextCache"/> 永远不存在
        /// 因此在此处进行必然的一次实现
        WndProc(msg => { return true; });
    }

    /// <summary>
    /// 消息循环
    /// </summary>
    /// <param name="wndProc">消息,true不拦截回调</param>
    [DebuggerStepThrough]
    public void WndProc(Func<System.Windows.Forms.Message, bool> wndProc)
    {
        _nativeCallProc.WndProc(msg => {
            WaitOne();
            return wndProc.Invoke(msg);
        });
    }

    /// <summary>
    /// 触发刷新,避免卡死<br/>
    /// 需要在其他线程中实现,若先利用 Send/Post (切到cad主线程) 会导致卡死;<br/>
    /// 0x01 注册表自动load的时候,鼠标在绘图区中间或者界面外,<br/>
    /// 0x02 netload命令的时候,<see cref="Handle"/>的界面已经完成,<br/>
    /// 这两种情况,将永远等不到当前上下文切换,导致上下文缓存会一直为null<br/>
    /// 此函数将触发当前上下文切换<br/>
    /// 0x03 当上下文缓存不为null,那么当前上下文是null,也需要触发<see cref="WM.WM_NCCALCSIZE"/>,否则获取cad当前文档是null<br/>
    /// 因为需要窗口的Rect信息<br/>
    /// 所以不能自己进行:<br/>
    /// <![CDATA[
    ///    SendMessage(AutoGo.AcapMidSyncEx.Handle,
    ///                WM.WM_NCCALCSIZE,
    ///                (IntPtr)0x01,
    ///                rect/*NCCALCSIZE_PARAMS*/)
    /// ]]><br/>
    /// </summary>
    public void Refresh()
    {
        WindowsAPI.SetWindowPos(Handle);
    }

    #region IDisposable接口相关函数
    public bool IsDisposed { get; private set; } = false;

    /// <summary>
    /// 手动调用释放
    /// </summary>
    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    /// <summary>
    /// 析构函数调用释放
    /// </summary>
    ~SyncEx()
    {
        Dispose(false);
    }

    protected virtual void Dispose(bool disposing)
    {
        // 不重复释放
        if (IsDisposed) return;
        IsDisposed = true;

        _nativeCallProc.Dispose();
    }
    #endregion
}

public class SyncEx2Form : Basal.SyncContext
{
    public System.Windows.Forms.Form? Window;

    /// <summary>
    /// 处理Form上下文
    /// </summary>
    /// <param name="window"></param>
    public SyncEx2Form(System.Windows.Forms.Form window)
    {
        Window = window ?? throw new ArgumentNullException(nameof(window));
    }

    /// <summary>
    /// 加载事件,因为显示之后才确定分配了线程
    /// </summary>
    public void Loaded()// object? sender = null/*, RoutedEventArgs? e = null*/
    {
        // 空闲时候执行的事件-cad08直接非Debug用不了
        System.Windows.Forms.Application.Idle += (sender, e) => {
            WaitOne();
        };
    }
}