namespace JoinBox.Forms;

using JoinBox.Basal;
using System;
using System.IO;
using System.Windows.Forms;
using System.Diagnostics;

public class Clipboard2
{
    /// <summary>
    /// e大写的 获取剪贴板文件路径
    /// 此处代码陈旧,去IFox的tests看最新的...
    /// </summary>
    /// <returns></returns>
    public string? GetClipboardFilePath()
    {
        // 得到可用格式的列表
        var dataObject = Clipboard.GetDataObject();
        var formats = dataObject.GetFormats();
        for (int i = 0; i < formats.Length; i++)
        {
            var format = formats[i];
            if (!format.StartsWith("AutoCAD."))
                continue;
            using var stream = dataObject.GetData(format) as MemoryStream;
            if (stream == null)
                return null;
            using StreamReader reader = new(stream, System.Text.Encoding.Unicode);
            if (reader == null)
                return null;

            char[] text = new char[260];
            reader.Read(text, 0, 260);
            // 我们清理有大量的0字符数组中的值
            return new string(text).TrimEnd(new char[] { '0' });
        }
        return null;
    }
}


#if true
// c#事件检查剪贴板 Clipboard
// https://www.cnblogs.com/crwy/p/11257725.html

/// <summary>
/// 更新剪贴板内容时提供通知
/// </summary>
public sealed class ClipboardNotification
{
    /// <summary>
    /// 更新剪贴板的内容时发生
    /// </summary>
    public event MethodInvoker? ClipboardUpdate;
    public ClipboardNotification()
    {
        new NotificationForm(OnClipboardUpdate);
    }
    /// <summary>
    /// 提高了 <see cref="ClipboardUpdate"/> 事件.
    /// </summary>
    private void OnClipboardUpdate()
    {
        ClipboardUpdate?.Invoke();
    }
}
class NotificationForm : Form
{
    readonly IntPtr _hWnd;
    readonly MethodInvoker? _onClipboardUpdate;
    public NotificationForm(MethodInvoker? onClipboardUpdate)
    {
        _hWnd = Handle;
        ClipTool.AddClipboardFormatListener(_hWnd);

        _onClipboardUpdate = onClipboardUpdate;
    }

    ~NotificationForm()
    {
        ClipTool.RemoveClipboardFormatListener(this._hWnd);
    }

    protected override void WndProc(ref Message m)
    {
        if (m.Msg != (int)WM.WM_CLIPBOARDUPDATE)
            base.WndProc(ref m);
        try
        {
            string str = DataFormats.Text;
            str = DataFormats.OemText;
            str = DataFormats.MetafilePict;    // 图元文件Pict
            str = DataFormats.EnhancedMetafile;// 增强型图元文件
            str = DataFormats.Bitmap;          // 位图
            str = DataFormats.Dib;             // 设备无关的位图 这个还有信息

            if (Clipboard.ContainsAudio())
            {
            }
            else if (Clipboard.ContainsData(str))
            {
                var s = Clipboard.GetData(str);
                MessageBox.Show(str); // 获取视口信息,但是这里截获不到啊
            }
            else if (Clipboard.ContainsFileDropList())
            {
            }
            else if (Clipboard.ContainsImage())
            {
            }
            else if (Clipboard.ContainsText())// 复制文本成功
            {
                string strdata = Clipboard.GetText();// 获取文本信息
                if (strdata.StartsWith("http://") || strdata.StartsWith("https://"))
                {
                    MessageBox.Show(string.Format("在{0}剪贴板发生了变化。\r\n" +
                       "包含URI：{1}" +
                       "\r\n---------------------------------------------\r\n",
                       DateTime.Now.ToString("HH:mm:ss"),
                       strdata));

                    // 返回0表示已经处理消息
                    m.Result = IntPtr.Zero;
                }
            }
        }
        catch (Exception)
        {
        }
    }
}
#endif


#if true2
public class InterceptKeys
{
    /// <summary>
    /// 设置默认值与数据
    /// </summary>
    /// <param name="initInt"></param>
    /// <param name="outString">"F:\图片\精品分类\品牌图片\google徽标\@_@.gif"</param>
    public static void Init(int initInt, string outString)
    {
        i = initInt;
        str = outString;
    }

    static int i = 0;
    static string str = @"I:\over\Images\google\@_@.jpg";

    static IntPtr HookCallback(int nCode, int wParam, IntPtr lParam)
    {
        if (nCode >= 0 && wParam == (int)WM.WM_KEYDOWN)
        {
            Keys key = (Keys)lParam;
            // 同时按下Ctrl+V键的时候
            if (Control.ModifierKeys == Keys.Control && key.Equals(Keys.V))
            {
                // 获得剪切板数据
                string data = Clipboard.GetText(TextDataFormat.UnicodeText);
                // 重新设置剪切板数据
                DataObject m_data = new();
                m_data.SetData(DataFormats.Text, true, str.Replace("@_@", Convert.ToString(++i)));
                Clipboard.SetDataObject(m_data, true);
            }
        }
        return WindowsAPI.CallNextHookEx(_hookID, nCode, wParam, lParam);
    }
}
#endif