﻿namespace JoinBox.Forms;

using System.Security.Principal;

public static class ProcessHelper
{
    /// <summary>
    /// 判断当前程序是否通过管理员运行
    /// </summary>
    /// <returns></returns>
    public static bool IsAdministrator()
    {
        var current = WindowsIdentity.GetCurrent();
        var windowsPrincipal = new WindowsPrincipal(current);
        return windowsPrincipal.IsInRole(WindowsBuiltInRole.Administrator);
    }
}
