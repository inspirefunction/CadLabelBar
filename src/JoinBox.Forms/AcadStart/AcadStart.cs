﻿namespace JoinBox.Forms;

using System;
using System.Diagnostics;
using System.Threading;
using Application = System.Windows.Forms.Application;

#if NET5_0
using Microsoft.VisualBasic;
#else
using System.Runtime.InteropServices;
#endif

public static class StartApplicat
{
    public static object Run(string progID, string exePath)
    {
        // 处理 GetActiveObject 在电脑睡眠之后获取就会失败.所以要 ProcessStartInfo
        // https://blog.csdn.net/yuandingmao/article/details/5558763?_t_t_t=0.8027849649079144
        // string progID = "AutoCAD.Application.17.1";
        // string exePath = @"C:\Program Files (x86)\AutoCAD 2008\acad.exe";

        var acadApp = AcadStartBlocking(progID);
        if (acadApp is not null)
            return acadApp;

        // var psi = new ProcessStartInfo(exePath, "/p myprofile");// 使用cad配置,myprofile是配置名称,默认就不写
        var tempPath = Environment.GetEnvironmentVariable("TEMP");
        if (tempPath is not null)
        {
            var psi = new ProcessStartInfo(exePath, "/nologo")
            {
                WorkingDirectory = tempPath// 这里什么路径都可以的
            };
            var pr = Process.Start(psi);
            pr?.WaitForInputIdle();// 这里并不会阻塞
        }

        int qidong = 0;
        while (acadApp is null)
        {
            try
            {
                acadApp = AcadStartBlocking(progID);
            }
            catch
            {
                Application.DoEvents();
            }
            if (qidong == 20)
                throw new ArgumentException("启动失败,错误计数已达最高");

            ++qidong;
            Thread.Sleep(500);
        }
        return acadApp;
    }


    /// <summary>
    /// 启动cad(阻塞)
    /// </summary>
    /// <returns></returns>
    public static object? AcadStartBlocking(string ProgID, string? path = null)
    {
        object? acadApp = null;
#if NET5_0
        // 两种方法,都会启动一个新的Acad.
        int startWay = 1;// 启动方式
        if (startWay == 1)
        {
            acadApp = Interaction.CreateObject(ProgID);
        }
        else if (startWay == 2)
        {
            var acadComType = Type.GetTypeFromProgID(ProgID);
            if (acadComType is null)
                throw new ArgumentNullException($"本机不存在:{ProgID}");
            acadApp = Activator.CreateInstance(acadComType);
        }
#else
        acadApp = Marshal.GetActiveObject(ProgID);
#endif
        return acadApp;
    }

    /// <summary>
    /// 启动cad(非阻塞)
    /// </summary>
    /// <returns>进程id</returns>
    public static int AcadStartAntiBlocking(string exePath)
    {
#if NET5_0
        // 不阻塞启动
        // var exePath   = @"C:\Program Files\Autodesk\AutoCAD 2021\acad.exe";
        // exePath       = @"C:\Program Files (x86)\AutoCAD 2008\acad.exe";
        int acadId = Interaction.Shell(exePath, AppWinStyle.NormalFocus); // 正常大小启动,返回进程id
        return acadId;
#else
        // var psi = new ProcessStartInfo(exePath, "/p myprofile");// 使用cad配置,myprofile是配置名称,默认就不写
        var psi = new ProcessStartInfo(exePath, "/nologo")
        {
            WorkingDirectory = Environment.GetEnvironmentVariable("TEMP")// 这里什么路径都可以的
        };
        var pr = Process.Start(psi);
        //  pr.WaitForInputIdle();// 这里并不会阻塞
        return pr.Id;
#endif
    }

}
