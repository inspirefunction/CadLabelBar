﻿namespace JoinBox.Forms;

using System;
using Microsoft.Win32;


public class FileTypeRegInfo
{
    /// <summary>
    /// 扩展名
    /// </summary>
    public string ExtendName;
    /// <summary>
    /// 说明
    /// </summary>
    public string? Description;
    /// <summary>
    /// 关联的图标
    /// </summary>
    public string? IconPath;
    /// <summary>
    /// 应用程序路径
    /// </summary>
    public string? ProjectPath;

    /// <summary>
    /// 启动的exe名称
    /// </summary>
    public string RelationName => ExtendName.Substring(1, ExtendName.Length - 1) + "Project";

    /// <summary>
    /// 文件类型注册信息
    /// </summary>
    /// <param name="extendName">后缀名,带点</param>
    public FileTypeRegInfo(string? extendName)
    {
        if (extendName is null)
            throw new ArgumentNullException(nameof(extendName));

        ExtendName = extendName;
    }

    /// <summary>
    /// 使文件类型与对应的图标及应用程序关联起来
    /// </summary>
    public void RegisterFileType()
    {
        // 在windows服务中,如果你不手工删除一次{/计算机\HKEY_CLASSES_ROOT\.vlx}注册表,
        // 那么以下代码将会有很奇怪的表现,
        // 你通过这里的代码无法删除.vlx注册表项.
        // 你添加在SetValue("",relationName)默认项的在程序结束后也不会呈现,但立即GetValue("")却可以获取值..
        // 手工删除一次,即可解决所有的问题..

        // 计算机\HKEY_CLASSES_ROOT\.vlx 上面写引用的注册程序vlxProject
        var fileTypeKey = Registry.ClassesRoot.CreateSubKey(this.ExtendName);
        if (fileTypeKey is not null)
        {
            fileTypeKey.SetValue("", RelationName);// 默认位置就是""
            fileTypeKey.Close();
        }

        // 计算机\HKEY_CLASSES_ROOT\vlxProject 注册了一个打开程序,这个程序内写打开的信息
        var relationKey = Registry.ClassesRoot.CreateSubKey(RelationName);
        if (relationKey is not null)
        {
            if (Description is not null)
                relationKey.SetValue("", Description);
            var iconKey = relationKey.CreateSubKey("DefaultIcon");
            if (IconPath is not null)
                iconKey.SetValue("", IconPath.Replace("\\", "/"));

            var shellKey = relationKey.CreateSubKey("Shell");
            var openKey = shellKey.CreateSubKey("Open");
            var commandKey = openKey.CreateSubKey("Command");
            commandKey.SetValue("", $"\"{this.ProjectPath}\" \"%1\""); // " %1"表示将被双击的文件的路径传给目标应用程序
            relationKey.Close();
        }
    }


    /// <summary>
    /// 删除注册表项-后缀名
    /// </summary>
    public void DeleteRegExtend()
    {
        var fileTypeKey = Registry.ClassesRoot.OpenSubKey(ExtendName, true);
        if (fileTypeKey is not null)
        {
            fileTypeKey.DeleteValue("");
            fileTypeKey.Close();
        }
    }

    /// <summary>
    /// 删除注册表项-转发器程序
    /// </summary>
    public void DeleteRegRelation()
    {
        var fileTypeKey = Registry.ClassesRoot.OpenSubKey(RelationName);
        if (fileTypeKey is not null)
            Registry.ClassesRoot.DeleteSubKeyTree(RelationName);
    }

    /// <summary>
    /// 更新指定文件类型关联信息
    /// </summary>
    public bool UpdateFileTypeRegInfo()
    {
        if (!FileTypeRegistered())
            return false;

        var relationKey = Registry.ClassesRoot.OpenSubKey(RelationName, true);
        if (relationKey is not null)
        {
            if (Description is not null)
                relationKey.SetValue("", Description);
            var iconKey = relationKey.OpenSubKey("DefaultIcon", true);
            if (IconPath is not null)
                iconKey?.SetValue("", IconPath);

            var shellKey = relationKey.OpenSubKey("Shell");
            var openKey = shellKey?.OpenSubKey("Open");
            var commandKey = openKey?.OpenSubKey("Command", true);
            commandKey?.SetValue("", $"\"{ProjectPath}\" \"%1\""); // " %1"表示将被双击的文件的路径传给目标应用程序
            relationKey.Close();
        }
        return true;
    }


    /// <summary>
    /// 获取指定文件类型关联信息
    /// </summary>
    public FileTypeRegInfo GetFileTypeRegInfo()
    {
        var relationKey = Registry.ClassesRoot.OpenSubKey(RelationName);
        if (relationKey is not null)
        {
            Description = relationKey.GetValue("")?.ToString();
            var iconKey = relationKey.OpenSubKey("DefaultIcon");
            IconPath = iconKey?.GetValue("")?.ToString();
            var shellKey = relationKey.OpenSubKey("Shell");
            var openKey = shellKey?.OpenSubKey("Open");
            var commandKey = openKey?.OpenSubKey("Command");
            var temp = commandKey?.GetValue("")?.ToString();
            ProjectPath = temp?.Substring(0, temp.Length - 3);// temp?[0..^3];
        }
        return this;
    }

    /// <summary>
    /// 指定文件类型是否已经注册
    /// </summary>
    public bool FileTypeRegistered()
    {
        var softwareKey = Registry.ClassesRoot.OpenSubKey(ExtendName);
        return softwareKey is not null;
    }
}
