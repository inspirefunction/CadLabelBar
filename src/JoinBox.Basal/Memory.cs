﻿namespace JoinBox.Basal;


using System;
using System.Runtime.InteropServices;


/// <summary>
/// 释放内存
/// </summary>
public class Memory
{
    /* https://www.cnblogs.com/kex1n/p/4043901.html
    使用这个函数来设置应用程序最小和最大的运行空间,只会保留需要的内存
    当应用程序被闲置或系统内存太低时,操作系统会自动调用这个机制来设置应用程序的内存
    应用程序也可以使用 VirtualLock 锁住一定范围的内存不被系统释放
    事实上,使用该函数并不能提高什么性能,也不会真的节省内存
    因为他只是暂时的将应用程序占用的内存移至虚拟内存,
    一旦,应用程序被激活或者有操作请求时,这些内存又会被重新占用。
    如果你强制使用该方法来 设置程序占用的内存,
    那么可能在一定程度上反而会降低系统性能,
    因为系统需要频繁的进行内存和硬盘间的页面交换。
    */

    [DllImport("kernel32.dll")]
    static extern bool SetProcessWorkingSetSize(IntPtr proc, int min, int max);

    /// <summary>
    /// 刷新内存
    /// 放在windowsLoaded事件
    /// 当我们的应用程序刚刚加载完成时,可以使用该操作一次,来将加载过程不需要的代码放到虚拟内存,这样,程序加载完毕后,保持较大的可用内存.
    /// </summary>
    public void FlushMemory()
    {
        GC.Collect();
        GC.WaitForPendingFinalizers();
        if (Environment.OSVersion.Platform == PlatformID.Win32NT)
        {
            SetProcessWorkingSetSize(System.Diagnostics.Process.GetCurrentProcess().Handle, -1, -1);
        }
    }
}