﻿namespace JoinBox.Basal;

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

public partial class ClipTool
{
    /// <summary>
    /// 侦听剪贴板
    /// </summary>
    /// <param name="hWnd"></param>
    /// <returns></returns>
    [DllImport("user32.dll", SetLastError = true)]
    public static extern bool AddClipboardFormatListener(IntPtr hWnd);
    /// <summary>
    /// 移除侦听剪贴板
    /// </summary>
    /// <param name="hWnd"></param>
    /// <returns></returns>
    [DllImport("user32.dll", SetLastError = true)]
    public static extern bool RemoveClipboardFormatListener(IntPtr hWnd);
    /// <summary>
    /// 将CWnd加入一个窗口链
    /// 每当剪贴板的内容发生变化时,就会通知这些窗口
    /// </summary>
    /// <param name="hWndNewViewer">句柄</param>
    /// <returns>返回剪贴板观察器链中下一个窗口的句柄</returns>
    [DllImport("User32.dll")]
    public static extern int SetClipboardViewer(IntPtr hWndNewViewer);
    /// <summary>
    /// 从剪贴板链中移出的窗口句柄
    /// </summary>
    /// <param name="hWndRemove">从剪贴板链中移出的窗口句柄</param>
    /// <param name="hWndNewNext">hWndRemove的下一个在剪贴板链中的窗口句柄</param>
    /// <returns>如果成功,非零;否则为0。</returns>
    [DllImport("User32.dll", CharSet = CharSet.Auto)]
    public static extern bool ChangeClipboardChain(IntPtr hWndRemove, IntPtr hWndNewNext);


    /// <summary>
    /// 锁定内存
    /// </summary>
    /// <param name="hMem"></param>
    /// <returns></returns>
    [DllImport("kernel32.dll", SetLastError = true)]
    static extern IntPtr GlobalLock(IntPtr hMem);
    /// <summary>
    /// 解锁内存
    /// </summary>
    /// <param name="hMem"></param>
    /// <returns></returns>
    [DllImport("kernel32.dll", SetLastError = true)]
    static extern bool GlobalUnlock(IntPtr hMem);
    /// <summary>
    /// 从堆中分配内存
    /// </summary>
    /// <param name="uFlags">分配方式</param>
    /// <param name="dwBytes">分配的字节数</param>
    /// <returns></returns>
    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern IntPtr GlobalAlloc(uint uFlags, int dwBytes);
    /// <summary>
    /// 释放堆内存
    /// </summary>
    /// <param name="hMem">由<see cref="GlobalAlloc"/>产生的句柄</param>
    /// <returns></returns>
    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern IntPtr GlobalFree(IntPtr hMem);
    /// <summary>
    /// 获取内存块大小
    /// </summary>
    /// <param name="hMem"></param>
    /// <returns></returns>
    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern uint GlobalSize(IntPtr hMem);


    /// <summary>
    /// 开启剪贴板<br/>
    /// 如果另一个窗口已经打开剪贴板,函数会失败.每次成功调用后都应有<see cref="CloseClipboard"/>调用.
    /// </summary>
    /// <param name="hWndNewOwner"></param>
    /// <returns></returns>
    [DllImport("user32.dll", SetLastError = true)]
    public static extern bool OpenClipboard(IntPtr hWndNewOwner);
    /// <summary>
    /// 关闭剪贴板
    /// </summary>
    /// <returns></returns>
    [DllImport("user32.dll", SetLastError = true)]
    public static extern bool CloseClipboard();
    /// <summary>
    /// 根据数据格式获取剪贴板
    /// </summary>
    /// <param name="lpszFormat">数据格式名称</param>
    /// <returns></returns>
    [DllImport("user32.dll", SetLastError = true)]
    public static extern uint RegisterClipboardFormat(string lpszFormat);
    /// <summary>
    /// 获取剪贴板
    /// </summary>
    /// <param name="uFormat">通常为<see cref="ClipboardFormat"/>但是cad有自己的位码</param>
    /// <returns></returns>
    [DllImport("user32.dll", SetLastError = true)]
    public static extern IntPtr GetClipboardData(uint uFormat);
    /// <summary>
    /// 设置剪贴板
    /// </summary>
    /// <param name="uFormat">通常为<see cref="ClipboardFormat"/>但是cad有自己的位码</param>
    /// <param name="hMem"></param>
    /// <returns></returns>
    [DllImport("user32.dll", SetLastError = true)]
    public static extern IntPtr SetClipboardData(uint uFormat, IntPtr hMem);
    /// <summary>
    /// 清空剪切板并释放剪切板内数据的句柄
    /// </summary>
    /// <returns></returns>
    [DllImport("user32.dll", SetLastError = true)]
    public static extern bool EmptyClipboard();
    /// <summary>
    /// 枚举剪贴板内数据类型
    /// </summary>
    /// <param name="format"></param>
    /// <returns></returns>
    [DllImport("user32.dll", SetLastError = true)]
    public static extern uint EnumClipboardFormats(uint format);


    /// <summary>
    /// 查找主线程<br/>
    /// 代替<see cref="AppDomain.GetCurrentThreadId()"/><br/>
    /// 托管线程和他们不一样: <see cref="Thread.CurrentThread.ManagedThreadId"/>
    /// </summary>
    /// <param name="hWnd">主窗口</param>
    /// <param name="lpdwProcessId">进程ID</param>
    /// <returns>线程ID</returns>
    [DllImport("user32.dll", SetLastError = true)]
    public static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);

    /// <summary>
    /// 锁定和释放内存
    /// </summary>
    /// <param name="data">锁定数据对象指针</param>
    /// <param name="task">返回锁定的内存片段指针,锁定期间执行任务</param>
    /// <returns>是否锁定成功</returns>
    /// <exception cref="ArgumentNullException"></exception>
    public static bool GlobalLockTask(IntPtr data, Action<IntPtr> task)
    {
        if (task == null)
            throw new ArgumentNullException(nameof(task));

        if (data == IntPtr.Zero)
            return false;

        try
        {
            var ptr = GlobalLock(data);

            // 有几率导致无效锁定:
            // 重复复制同一个图元时,第二次是 IntPtr.Zero,
            // 第三次就又可以复制了
            if (ptr == IntPtr.Zero)
                return false;

            task.Invoke(ptr);
        }
        finally
        {
            GlobalUnlock(data);
        }
        return true;
    }
}

public partial class ClipTool
{
    // https://blog.csdn.net/vencon_s/article/details/46345083

    /// <summary>
    /// 剪贴板数据保存目标数据列表
    /// </summary>
    static readonly List<byte[]> _bytes = new();
    /// <summary>
    /// 剪贴板数据类型列表
    /// </summary>
    static readonly List<uint> _formats = new();

    /// <summary>
    /// 遍历剪贴板保存内容
    /// </summary>
    /// <returns>真为成功,false为失败</returns>
    public static bool SaveClip()
    {
        if (OpenClipboard(IntPtr.Zero))
            return false;

        _bytes.Clear();
        _formats.Clear();

        uint cf = 0;
        while (true)
        {
            cf = EnumClipboardFormats(cf);// 枚举剪贴板所有数据类型
            if (cf == 0)
                break;

            _formats.Add(cf);
            IntPtr hMem = GetClipboardData(cf);

            GlobalLockTask(hMem, prt => {
                uint size = GlobalSize(hMem);
                if (size > 0)
                {
                    var buffer = new byte[size];
                    Marshal.Copy(prt, buffer, 0, buffer.Length);// 将剪贴板数据保存到自定义字节数组
                    _bytes.Add(buffer);
                }
            });
        }
        CloseClipboard();

        return _formats.Count > 0;
    }

    /// <summary>
    /// 恢复保存的数据
    /// </summary>
    /// <returns></returns>
    public static bool RestoreClip()
    {
        if (_formats.Count <= 0)
            return false;
        if (OpenClipboard(IntPtr.Zero))
            return false;

        for (int i = 0; i < _formats.Count; i++)
        {
            int size = _bytes[i].Length;
            IntPtr si = Marshal.AllocHGlobal(size);
            if (size > 0)
                Marshal.Copy(_bytes[i], 0, si, size);
            SetClipboardData(_formats[i], si);
        }
        CloseClipboard();
        return _formats.Count > 0;
    }
}


/// <summary>
/// 剪贴板的CF,也就是它的key
/// </summary>
public enum ClipboardFormat : uint
{
    /// <summary>
    /// Text format. Each line ends with a carriage return/linefeed (CR-LF) combination. A null character signals
    /// the end of the data. Use this format for ANSI text.
    /// </summary>
    CF_TEXT = 1,

    /// <summary>
    /// A handle to a bitmap (<c>HBITMAP</c>).
    /// </summary>
    CF_BITMAP = 2,

    /// <summary>
    /// Handle to a metafile picture format as defined by the <c>METAFILEPICT</c> structure. When passing a
    /// <c>CF_METAFILEPICT</c> handle by means of DDE, the application responsible for deleting <c>hMem</c> should
    /// also free the metafile referred to by the <c>CF_METAFILEPICT</c> handle.
    /// </summary>
    CF_METAFILEPICT = 3,

    /// <summary>
    /// Microsoft Symbolic Link (SYLK) format.
    /// </summary>
    CF_SYLK = 4,

    /// <summary>
    /// Software Arts' Data Interchange Format.
    /// </summary>
    CF_DIF = 5,

    /// <summary>
    /// Tagged-image file format.
    /// </summary>
    CF_TIFF = 6,

    /// <summary>
    /// Text format containing characters in the OEM character set. Each line ends with a carriage return/linefeed
    /// (CR-LF) combination. A null character signals the end of the data.
    /// </summary>
    CF_OEMTEXT = 7,

    /// <summary>
    /// A memory object containing a <c>BITMAPINFO</c> structure followed by the bitmap bits.
    /// </summary>
    CF_DIB = 8,

    /// <summary>
    /// Handle to a color palette. Whenever an application places data in the clipboard that depends on or assumes
    /// a color palette, it should place the palette on the clipboard as well. If the clipboard contains data in
    /// the <see cref="CF_PALETTE"/> (logical color palette) format, the application should use the
    /// <c>SelectPalette</c> and <c>RealizePalette</c> functions to realize (compare) any other data in the
    /// clipboard against that logical palette. When displaying clipboard data, the clipboard always uses as its
    /// current palette any object on the clipboard that is in the <c>CF_PALETTE</c> format.
    /// </summary>
    CF_PALETTE = 9,

    /// <summary>
    /// Data for the pen extensions to the Microsoft Windows for Pen Computing.
    /// </summary>
    CF_PENDATA = 10,

    /// <summary>
    /// Represents audio data more complex than can be represented in a CF_WAVE standard wave format.
    /// </summary>
    CF_RIFF = 11,

    /// <summary>
    /// Represents audio data in one of the standard wave formats, such as 11 kHz or 22 kHz PCM.
    /// </summary>
    CF_WAVE = 12,

    /// <summary>
    /// Unicode text format. Each line ends with a carriage return/linefeed (CR-LF) combination. A null character
    /// signals the end of the data.
    /// </summary>
    CF_UNICODETEXT = 13,

    /// <summary>
    /// A handle to an enhanced metafile (<c>HENHMETAFILE</c>).
    /// </summary>
    CF_ENHMETAFILE = 14,

    /// <summary>
    /// A handle to type <c>HDROP</c> that identifies a list of files. An application can retrieve information
    /// about the files by passing the handle to the <c>DragQueryFile</c> function.
    /// </summary>
    CF_HDROP = 15,

    /// <summary>
    /// The data is a handle to the locale identifier associated with text in the clipboard. When you close the
    /// clipboard, if it contains <c>CF_TEXT</c> data but no <c>CF_LOCALE</c> data, the system automatically sets
    /// the <c>CF_LOCALE</c> format to the current input language. You can use the <c>CF_LOCALE</c> format to
    /// associate a different locale with the clipboard text.
    /// An application that pastes text from the clipboard can retrieve this format to determine which character
    /// set was used to generate the text.
    /// Note that the clipboard does not support plain text in multiple character sets. To achieve this, use a
    /// formatted text data type such as RTF instead.
    /// The system uses the code page associated with <c>CF_LOCALE</c> to implicitly convert from
    /// <see cref="CF_TEXT"/> to <see cref="CF_UNICODETEXT"/>. Therefore, the correct code page table is used for
    /// the conversion.
    /// </summary>
    CF_LOCALE = 16,

    /// <summary>
    /// A memory object containing a <c>BITMAPV5HEADER</c> structure followed by the bitmap color space
    /// information and the bitmap bits.
    /// </summary>
    CF_DIBV5 = 17,

    /// <summary>
    /// Owner-display format. The clipboard owner must display and update the clipboard viewer window, and receive
    /// the <see cref="ClipboardMessages.WM_ASKCBFORMATNAME"/>, <see cref="ClipboardMessages.WM_HSCROLLCLIPBOARD"/>,
    /// <see cref="ClipboardMessages.WM_PAINTCLIPBOARD"/>, <see cref="ClipboardMessages.WM_SIZECLIPBOARD"/>, and
    /// <see cref="ClipboardMessages.WM_VSCROLLCLIPBOARD"/> messages. The <c>hMem</c> parameter must be <c>null</c>.
    /// </summary>
    CF_OWNERDISPLAY = 0x0080,

    /// <summary>
    /// Text display format associated with a private format. The <c>hMem</c> parameter must be a handle to data
    /// that can be displayed in text format in lieu of the privately formatted data.
    /// </summary>
    CF_DSPTEXT = 0x0081,

    /// <summary>
    /// Bitmap display format associated with a private format. The <c>hMem</c> parameter must be a handle to
    /// data that can be displayed in bitmap format in lieu of the privately formatted data.
    /// </summary>
    CF_DSPBITMAP = 0x0082,

    /// <summary>
    /// Metafile-picture display format associated with a private format. The <c>hMem</c> parameter must be a
    /// handle to data that can be displayed in metafile-picture format in lieu of the privately formatted data.
    /// </summary>
    CF_DSPMETAFILEPICT = 0x0083,

    /// <summary>
    /// Enhanced metafile display format associated with a private format. The <c>hMem</c> parameter must be a
    /// handle to data that can be displayed in enhanced metafile format in lieu of the privately formatted data.
    /// </summary>
    CF_DSPENHMETAFILE = 0x008E,

    /// <summary>
    /// Start of a range of integer values for application-defined GDI object clipboard formats. The end of the
    /// range is <see cref="CF_GDIOBJLAST"/>. Handles associated with clipboard formats in this range are not
    /// automatically deleted using the <c>GlobalFree</c> function when the clipboard is emptied. Also, when using
    /// values in this range, the <c>hMem</c> parameter is not a handle to a GDI object, but is a handle allocated
    /// by the <c>GlobalAlloc</c> function with the <c>GMEM_MOVEABLE</c> flag.
    /// </summary>
    CF_GDIOBJFIRST = 0x0300,

    /// <summary>
    /// See <see cref="CF_GDIOBJFIRST"/>.
    /// </summary>
    CF_GDIOBJLAST = 0x03FF,

    /// <summary>
    /// Start of a range of integer values for private clipboard formats. The range ends with
    /// <see cref="CF_PRIVATELAST"/>. Handles associated with private clipboard formats are not freed
    /// automatically, the clipboard owner must free such handles, typically in response to the
    /// <see cref="ClipboardMessages.WM_DESTROYCLIPBOARD"/> message.
    /// </summary>
    CF_PRIVATEFIRST = 0x0200,

    /// <summary>
    /// See <see cref="CF_PRIVATEFIRST"/>.
    /// </summary>
    CF_PRIVATELAST = 0x02FF,
}