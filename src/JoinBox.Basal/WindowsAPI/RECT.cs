﻿namespace JoinBox.Basal;
using System.Text;
using System.Drawing;

public partial class WindowsAPI
{
    public struct IntRect
    {
        /// <summary>
        /// 左
        /// </summary>
        public int Left;
        /// <summary>
        /// 上
        /// </summary>
        public int Top;
        /// <summary>
        /// 右
        /// </summary>
        public int Right;
        /// <summary>
        /// 下
        /// </summary>
        public int Bottom;

        public IntRect(int left, int top, int right, int bottom)
        {
            Left = left;
            Top = top;
            Right = right;
            Bottom = bottom;
        }

        /// <summary>
        /// 面积
        /// </summary>
        /// <returns></returns>
        public int Area => Width * Height;
        /// <summary>
        /// 宽度
        /// </summary>
        public int Width => Right - Left;
        /// <summary>
        /// 高度
        /// </summary>
        public int Height => Bottom - Top;

        /// <summary>
        /// 叉乘,计算 |p1 p2| X |p1 p|
        /// </summary>
        /// <param name="a">直线点1</param>
        /// <param name="b">直线点2</param>
        /// <param name="p">判断是否在内的点</param>
        /// <returns></returns>
        /// https://blog.csdn.net/pdcxs007/article/details/51436483
        static double Cross(POINT a, POINT b, POINT p)
        {
            return (b.X - a.X) * (p.Y - a.Y) - (p.X - a.X) * (b.Y - a.Y);
        }

        /// <summary>
        /// 中间
        /// </summary>
        /// <returns></returns>
        public POINT Mid => new(Left + (Right - Left) / 2, Top + (Bottom - Top) / 2);

        /// <summary>
        /// 左下
        /// </summary>
        /// <returns></returns>
        public POINT Min => new(Left, Bottom);
        /// <summary>
        /// 左上
        /// </summary>
        /// <returns></returns>
        public POINT MinEx => new(Left, Top);
        /// <summary>
        /// 右上
        /// </summary>
        /// <returns></returns>
        public POINT Max => new(Right, Top);
        /// <summary>
        /// 右下
        /// </summary>
        /// <returns></returns>
        public POINT MaxEx => new(Right, Bottom);

        /// <summary>
        /// 点在矩形内
        /// </summary>
        /// <param name="p">判断的点</param>
        public bool IsPointInMatrix(POINT p)
        {
            var p1 = Min;
            var p2 = MinEx;
            var p3 = Max;
            var p4 = MaxEx;

            return Cross(p1, p2, p) * Cross(p3, p4, p) >= 0 && Cross(p2, p3, p) * Cross(p4, p1, p) >= 0;
        }

        /// <summary>
        /// 有效的
        /// </summary>
        public bool IsValid => Left > 0 && Top > 0 && Right > 0 && Bottom > 0;

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("(Left:");
            sb.Append(Left);
            sb.Append(",Top:");
            sb.Append(Top);
            sb.Append(",Right:");
            sb.Append(Right);
            sb.Append(",Bottom:");
            sb.Append(Bottom);
            sb.Append(")");
            return sb.ToString();
        }

        public Rectangle Rectangle => new(Left, Top, Width, Height);
        public Size Size => new(Width, Height);
    }
}