﻿namespace JoinBox.Basal;

using System;
using System.Diagnostics;
using System.Windows.Forms;

/// <summary>
/// 键盘钩子
/// </summary>
public class KeyboardHook : IDisposable
{
    /// <summary>
    /// 键盘组合键同时按下
    /// </summary>
    public event KeyEventHandler? KeyDownEvent;
    /// <summary>
    /// 键盘按住
    /// </summary>
    public event KeyPressEventHandler? KeyPressEvent;
    /// <summary>
    /// 键盘弹起
    /// </summary>
    public event KeyEventHandler? KeyUpEvent;

    bool _isHookBreak = false;
    /// <summary>
    /// 否决本次输入:设置不向下回调
    /// </summary>
    public void Vote()
    {
        _isHookBreak = true;
    }

    /// 不要试图省略此变量,否则将会导致GC变量池满后释放<br/>
    /// 提示:激活 CallbackOnCollectedDelegate 托管调试助手(MDA)
    internal static WindowsAPI.CallBack? HookProc;
    internal static IntPtr _NextHookProc;//挂载成功的标记
    public readonly Process Process;

    /// <summary>
    /// 安装键盘钩子<br/>
    /// </summary>
    /// <param name="setLowLevel">低级钩子超时时间</param>
    public KeyboardHook(int setLowLevel = 25000)
    {
        _NextHookProc = IntPtr.Zero;
        Process = Process.GetCurrentProcess();
        WindowsAPI.CheckLowLevelHooksTimeout(setLowLevel);
    }

    void UnHook()
    {
        if (_NextHookProc != IntPtr.Zero)
        {
            WindowsAPI.UnhookWindowsHookEx(_NextHookProc);
            _NextHookProc = IntPtr.Zero;
        }
    }

    /// <summary>
    /// 设置钩子
    /// </summary>
    /// <param name="processHook">进程钩子true,全局钩子</param>
    public void SetHook(bool processHook = false)
    {
        UnHook();
        if (_NextHookProc != IntPtr.Zero)
            return;

        if (processHook)
        {
            HookProc = (nCode, wParam, lParam) => {
                if (nCode >= 0 && HookTask(nCode, wParam, lParam))
                    return (IntPtr)1;
                return WindowsAPI.CallNextHookEx(_NextHookProc, nCode, wParam, lParam);
            };
            _NextHookProc = WindowsAPI.SetWindowsHookEx(HookType.WH_KEYBOARD, HookProc,
                                                        IntPtr.Zero, (int)WindowsAPI.GetCurrentThreadId());
        }
        else
        {
            var moduleHandle = WindowsAPI.GetModuleHandle(Process.MainModule.ModuleName);
            HookProc = (nCode, wParam, lParam) => {
                if (nCode >= 0 && HookTask(nCode, wParam, lParam))
                    return (IntPtr)1;
                return WindowsAPI.CallNextHookEx(_NextHookProc, nCode, wParam, lParam);
            };
            _NextHookProc = WindowsAPI.SetWindowsHookEx(HookType.WH_KEYBOARD_LL, HookProc,
                                                        moduleHandle, 0);
        }
    }

    /// <summary>
    /// 钩子的消息处理
    /// </summary>
    /// <param name="nCode"></param>
    /// <param name="wParam"></param>
    /// <param name="lParam"></param>
    /// <returns>false不终止回调,true终止回调</returns>
    bool HookTask(int nCode, int wParam, IntPtr lParam)
    {
        _isHookBreak = false;

        // 侦听键盘事件挂载后才进行
        if (KeyDownEvent is null && KeyUpEvent is null && KeyPressEvent is null)
            return false;

        var wPa = (WM)wParam;

        // 键盘 组合键同时按下
        if (KeyDownEvent is not null && (wPa == WM.WM_KEYDOWN || wPa == WM.WM_SYSKEYDOWN))
        {
            var keyMsg = WindowsAPI.KeyboardHookStruct.Create(lParam);
            KeyDownEvent(this, new((Keys)keyMsg.VkCode));
        }

        // 键盘按下
        if (KeyPressEvent is not null && wPa == WM.WM_KEYDOWN)
        {
            byte[] keyState = new byte[256];
            WindowsAPI.GetKeyboardState(keyState);

            byte[] inBuffer = new byte[2];
            var keyMsg = WindowsAPI.KeyboardHookStruct.Create(lParam);
            if (WindowsAPI.ToAscii(keyMsg.VkCode, keyMsg.ScanCode, keyState, inBuffer, keyMsg.Flags) == 1)
                KeyPressEvent(this, new((char)inBuffer[0]));
        }

        // 键盘抬起
        if (KeyUpEvent is not null && (wPa == WM.WM_KEYUP || wPa == WM.WM_SYSKEYUP))
        {
            var keyMsg = WindowsAPI.KeyboardHookStruct.Create(lParam);
            KeyUpEvent(this, new((Keys)keyMsg.VkCode));
        }

        // 屏蔽此输入
        if (_isHookBreak)
            return true;

        return false;
    }

    #region IDisposable接口相关函数
    public bool IsDisposed { get; private set; } = false;

    /// <summary>
    /// 手动调用释放
    /// </summary>
    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    /// <summary>
    /// 析构函数调用释放
    /// </summary>
    ~KeyboardHook()
    {
        Dispose(false);
    }

    protected virtual void Dispose(bool disposing)
    {
        // 不重复释放,并设置已经释放
        if (IsDisposed) return;
        IsDisposed = true;

        UnHook();
    }
    #endregion
}