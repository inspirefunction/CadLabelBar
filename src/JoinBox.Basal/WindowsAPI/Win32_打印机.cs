﻿namespace JoinBox.Basal;

using System.Collections.Generic;
using System.Drawing.Printing;
using System.Runtime.InteropServices;

public partial class WindowsAPI
{
    /// <summary>
    /// 设置默认打印机
    /// </summary>
    /// <param name="Name"></param>
    /// <returns></returns>
    [DllImport("winspool.drv")]
    public static extern bool SetDefaultPrinter(string Name);


    /// <summary>
    /// 系统所有打印机名称(默认将在第一)
    /// </summary>
    public static void GetLocalPrinters(List<string> list)
    {
        // 读取默认打印机,放在数组第一个
        using (var pdoc = new PrintDocument())
        {
            var pn = pdoc.PrinterSettings.PrinterName;
            if (pn is not null && !string.IsNullOrEmpty(pn))
                list.Add(pn);
            else
                list.Add(null!);
        }

        // 遍历所有打印机名称
        var insp = PrinterSettings.InstalledPrinters;
        for (int i = 0; i < insp.Count; i++)
            if (!list.Contains(insp[i]))
                list.Add(insp[i]);
    }
}