﻿namespace JoinBox.Basal;

using System.Runtime.InteropServices;

public partial class WindowsAPI
{
    /// <summary>
    /// 写入配置文件字符串
    /// </summary>
    /// <param name="lpszSection"></param>
    /// <param name="lpszKeyName"></param>
    /// <param name="lpszString"></param>
    /// <returns></returns>
    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern int WriteProfileString(string lpszSection, string lpszKeyName, string lpszString);

    /// <summary>
    /// 添加字体资源
    /// </summary>
    /// <param name="lpFileName"></param>
    /// <returns></returns>
    [DllImport("gdi32.dll")]
    public static extern int AddFontResource(string lpFileName);
}