﻿namespace JoinBox.Basal;

using System;
using System.Collections.Generic;


public static class EnumerableUp
{
    /// <summary>
    /// 仿lisp的mapcar函数
    /// </summary>
    /// <typeparam name="T1">集合</typeparam>
    /// <typeparam name="T2">集合</typeparam>
    /// <typeparam name="TR"></typeparam>
    /// <param name="lst1"></param>
    /// <param name="lst2"></param>
    /// <param name="func"></param>
    /// <returns></returns>
    public static IEnumerable<TR> Mapcar<T1, T2, TR>(IEnumerable<T1> lst1, IEnumerable<T2> lst2, Func<T1, T2, TR> func)
    {
        var itor1 = lst1.GetEnumerator();
        var itor2 = lst2.GetEnumerator();
        while (itor1.MoveNext() && itor2.MoveNext())
            yield return func(itor1.Current, itor2.Current);
    }

    // public class TestMapcar
    // {
    //    [CommandMethod(nameof(ttmapmap))]
    //    public void ttmapmap()
    //    {
    //        Editor ed = Acap.DocumentManager.MdiActiveDocument.Editor;

    //        var st1 = new List<double> { 1, 2, 3, 4 };
    //        var st2 = new List<double> { 1, 2, 3, 4 };
    //        var numbers = JoinBox.BasalCurrency.EnumerableUp.Mapcar(st1, st2, (a, b) => a + b);

    //        foreach (var item in numbers)
    //        {
    //            ed.WriteMessage("\n" + item.ToString());
    //        }
    //    }
    // }

    // https://www.jianshu.com/p/476b112fd77a
    // 和下面等价
    // public IEnumerable<int> Method()
    // {
    //    List<int> results = new List<int>();
    //    int counter = 0;
    //    int result = 1;
    //    while (counter++ < 10)
    //    {
    //        result = result * 2;
    //        results.Add(result);
    //    }
    //    return results;
    // }
    // public IEnumerable<int> YieldDemo()
    // {
    //    int counter = 0;
    //    int result = 1;
    //    while (counter++ < 10)
    //    {
    //        result *= 2;
    //        yield return result;
    //    }
    // }
}