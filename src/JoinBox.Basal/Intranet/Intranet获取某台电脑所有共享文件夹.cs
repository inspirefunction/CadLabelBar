﻿namespace JoinBox.Basal;

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

// https://q.cnblogs.com/q/46971/
public partial class Intranet
{
    /// <summary>
    /// 共享资源信息
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    protected struct ShareInfo
    {
        [MarshalAs(UnmanagedType.LPWStr)]
        public string Name;
        [MarshalAs(UnmanagedType.U4)]
        public uint Type;
        [MarshalAs(UnmanagedType.LPWStr)]
        public string Remark;

        public static ShareInfo Create(IntPtr lParam)
        {
            return (ShareInfo)Marshal.PtrToStructure(lParam, typeof(ShareInfo));
        }
        public void ToPtr(IntPtr lParam)
        {
            Marshal.StructureToPtr(this, lParam, true);
        }
    }

    [DllImport("Netapi32.dll", CharSet = CharSet.Auto)]// EntryPoint = "NetShareEnum"
    protected static extern int NetShareEnum(
        [MarshalAs(UnmanagedType.LPWStr)] string servername,
        [MarshalAs(UnmanagedType.U4)] uint level,
        out IntPtr bufptr,
        [MarshalAs(UnmanagedType.U4)] int prefmaxlen,
        [MarshalAs(UnmanagedType.U4)] out uint entriesread,
        [MarshalAs(UnmanagedType.U4)] out uint totalentries,
        [MarshalAs(UnmanagedType.U4)] out uint resume_handle
    );
    /// <summary>
    /// 遍历某台电脑的共享文件
    /// </summary>
    /// <param name="server"></param>
    /// <returns></returns>
    public static string[]? GetNetShareList(string server)
    {
        //-1应该是获取所有的share,msdn里面的例子是这么写的,返回0表示成功
        if (NetShareEnum(server, 1, out IntPtr buffer, -1, out uint entriesread, out _, out _) != 0)
            return null;

        int ptr = buffer.ToInt32();
        var alShare = new List<string>();
        for (int i = 0; i < entriesread; i++)
        {
            var shareInfo = ShareInfo.Create(new IntPtr(ptr));
            if (shareInfo.Type == 0)// Disk drive类型
                alShare.Add(shareInfo.Name);
            ptr += Marshal.SizeOf(shareInfo);
        }
        string[] share = new string[alShare.Count];
        for (int i = 0; i < alShare.Count; i++)
            share[i] = alShare[i];

        return share;
    }
}