﻿namespace JoinBox.Basal;


using System;
using System.Collections;


/// <summary>
/// 迭代元素
/// </summary>
public class PublicEnumerator<T> : IEnumerator
{
    #region 成员
    public T[] _lnr;

    object? IEnumerator.Current => Current;

    // 推断类型从这里返回当前元素T
    public T Current
    {
        get
        {
            try
            {
                return _lnr[position];
            }
            catch (IndexOutOfRangeException)
            {
                throw new InvalidOperationException();
            }
        }
    }
    int position = -1;      // 计数
    #endregion

    #region 构造
    public PublicEnumerator(T[] arr)
    {
        _lnr = arr;
    }
    #endregion

    #region 方法
    public bool MoveNext()  // 迭代
    {
        position++;
        return position < _lnr.Length;
    }
    public void Reset()   // 初始计数赋值
    {
        position = -1;
    }
    #endregion
}
