﻿//#define DebugPrint

namespace JoinBox.Basal;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;

/// <summary>
/// 线程上下文
/// </summary>
public class SyncContext : SynchronizationContext
{
    #region 成员
    struct ContextTaskStruct
    {
        volatile public bool IsPost;
        volatile public Action Task;
    }

    readonly static List<ContextTaskStruct> _tasks = new();

    /// <summary>
    /// 当前上下文的缓存<br/>
    /// 它虽然是父类的Current(<see cref="SynchronizationContext.Current"/>)赋值的,但是不完全相等<br/>
    /// 因为父类的没有保留上次的能力<br/>
    /// 缓存的意义:<br/>
    /// 当使用cad的新建图纸对话框(其他对话框也是),此时 <see cref="SynchronizationContext.Current"/>==null,<br/>
    /// 若没有缓存的时候,会直接卡死<br/>
    /// </summary>
    static volatile SynchronizationContext? _contextCache;

    /// <summary>
    /// 存在上下文缓存
    /// </summary>
    public bool ExistContextCache => _contextCache != null;

    // 线程锁标记
    readonly static object _objLock = new();

    // 线程的开关_第一种
    readonly static ManualResetEvent _OnOff = new(true);// 一直放行,除非遇到信号
    // 线程的开关_第二种: https://article.itxueyuan.com/prodBZ
    // readonly AutoResetEvent OnOff = new(true);参数true就是放行状态,循环一次它就变阻塞了
    #endregion

    #region 方法
    /// <summary>
    /// 阻塞主线程<br/>
    /// 写在子类化的消息循环或者窗体空闲事件上会不断的循环它<br/>
    /// 然后才能使用Send和Post<br/>
    /// 但是Send会无法使用getpoint等交互函数,所以全部用Post
    /// </summary>
    protected void WaitOne()
    {
        // 拥有上下文的时候才阻塞主线程
        if (Current is null)
            return;
        // 接收到信号就会阻塞线程
        _OnOff.WaitOne();
        // 切换上下文
        _contextCache = Current;
    }

    /// <summary>
    /// 子线程发送同步上下文处理<br/>
    /// </summary>
    /// <param name="task">任务</param>
    public void Send(Action task)
    {
        SendOrPost(task, Send, false);
    }
    public override void Send(SendOrPostCallback d, object? state = null)
    {
        if (_contextCache == null)
            return;

        // 初始化时,此处偶发性卡死,因为send要放行主线程
        // 否则获取到 dm.MdiActiveDocument 就会卡死
        _OnOff.Set();  // 设置标记放行主线程(和post顺序不一样)
        _contextCache.Send(d, state);
    }

    /// <summary>
    /// 子线程发送异步上下文处理<br/>
    /// </summary>
    /// <param name="task">任务</param>
    public void Post(Action task)
    {
        SendOrPost(task, Post, true);
    }
    public override void Post(SendOrPostCallback d, object? state = null)
    {
        if (_contextCache == null)
            return;

        _contextCache.Post(d, state);
        _OnOff.Set();  // 设置标记放行主线程(和send顺序不一样)
    }


    /// <summary>
    /// 子线程调用发送同步/异步上下文<br/>
    /// <see cref="_OnOff.Reset"/>会发送信号给<see cref="WaitOne()"/>阻塞主线程
    /// </summary>
    /// <param name="task">任务</param>
    /// <param name="sendOrPost">同步/异步</param>
    void SendOrPost(Action task, Action<SendOrPostCallback, object?> sendOrPost, bool isPost)
    {
        if (_contextCache == null)
            return;

        // 设置标记阻塞主线程
        if (!_OnOff.Reset())
            return;

        if (isPost)
        {
            DebugPrintl("1.Post任务::锁定前");
            lock (_objLock)
            {
                // 储存当次
                _tasks.Add(new() { IsPost = isPost, Task = task });
                DebugPrintl("2.Post任务::锁定了执行任务前+");
                Task(sendOrPost);
                DebugPrintl("3.Post任务::锁定了执行任务后-");
            }
            DebugPrintl("4.Post任务::解锁了-------------------");
        }
        else
        {
            // 储存当次
            lock (_objLock)
                _tasks.Add(new() { IsPost = isPost, Task = task });
            DebugPrintl("1.Send任务::前+");
            // 同步任务需要主线程的消息循环,所以不能锁定
            Task(sendOrPost);
            DebugPrintl("2.Send任务::后-");
        }
    }

    static void Task(Action<SendOrPostCallback, object?> sendOrPost)
    {
        for (int i = 0; i < _tasks.Count; i++)
        {
            var task = _tasks[i];/*不要简化这句,多线程时它会放到线程变量上,否则post无法获取索引*/
            DebugPrintl($"任务{i}");
            sendOrPost.Invoke(d => {
                DebugPrintl($"任务{i}:++++");
                task.Task.Invoke();
                DebugPrintl($"任务{i}:----");
            }, null);
        }
        _tasks.Clear();
    }

    /// <summary>
    /// cad命令切换: Debugx
    /// </summary>
    /// <param name="message">打印信息</param>
    /// <param name="time">打印时间</param>
    [MethodImpl]
    [Conditional("DEBUG")]
    public static void DebugPrintl(object message, bool time = true)
    {
#if DebugPrint
        var flag = Environment.GetEnvironmentVariable("debugx", EnvironmentVariableTarget.User);
        if (flag == null || flag == "0")
            return;

        if (time)
            message = $"{DateTime.Now.ToLongDateString() + DateTime.Now.TimeOfDay}\n" +
            $"\t\tThreadId:{Thread.CurrentThread.ManagedThreadId}\n" +
            $"\t\t{message}";
#if DEBUG
        //System.Diagnostics.Debug.Indent();
        System.Diagnostics.Debug.WriteLine(message);
        //System.Diagnostics.Debug.Unindent();
#else
        //System.Diagnostics.Debug.Indent();
        System.Diagnostics.Trace.WriteLine(message);
        //System.Diagnostics.Debug.Unindent();
#endif
#endif
    }
    #endregion
}