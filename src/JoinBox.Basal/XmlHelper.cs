﻿namespace JoinBox.Basal;

public class XmlHelper
{
    // public delegate bool DelegateXml(XElement xElement);
    // public delegate void XmlVoid_EventHandler(params object[] obj);
    /// <summary>
    /// 获取xml数据,在委托上面实现处理
    /// </summary>
    /// <param name="ve">委托</param>
    /// <param name="xml_path">xml的文件路径</param>
    /// <param name="xml_ClassNames">数组,从根目录开始层层包裹</param>
    /// <returns>返回xml(用于保存数据)</returns>
    // public static XDocument GetXmla(string xml_path, DelegateXml xe)
    // {
    //    if (!File.Exists(xml_path))// 可能会被删除
    //    {
    //        throw new ArgumentException("不存在配置文件:" + xml_path);
    //    }
    //    var xdoc = XDocument.Load(xml_path); // 加载xml文件
    //    var xeles = xdoc.Root.Elements(); // 获取根元素下所有的直接子元素
    //    foreach (XElement item in xeles)
    //    {
    //        if (xe(item))
    //        {
    //            break;
    //        }
    //    }
    //    return xdoc;
    // }
#if false
    public static XDocument GetXml(XmlVoid_EventHandler ve, string xml_path, params string[] xml_ClassNames)
    {
        if (!File.Exists(xml_path))// 可能会被删除
        {
            throw new ArgumentException("不存在配置文件:" + xml_path);
        }
        var xdoc = XDocument.Load(xml_path); // 加载xml文件
        var xeles = xdoc.Root.Elements();    // 获取根元素下所有的直接子元素
        int classNumber = 0;
        Recursionet(xeles, xml_ClassNames, ref classNumber, ve);
        return xdoc;
    }
    /// <summary>
    /// 递归xml数据,在xml[n] == str[n],就输出词条,以获取词条名称和值
    /// </summary>
    /// <param name="xeles"></param>
    /// <param name="xml_ClassNames"></param>
    /// <param name="n"></param>
    /// <param name="ve"></param>
    static void Recursionet(IEnumerable<XElement> xeles, string[] xml_ClassNames, ref int n, XmlVoid_EventHandler ve)
    {
        foreach (XElement xeleClass in xeles)
        {
            if (xeleClass.Name == xml_ClassNames[n])
            {
                n++;
                if (n == xml_ClassNames.Length)
                {
                    var lst = xeleClass.Elements().ToList();
                    if (lst.Count == 0)
                    {
                        ve(xeleClass);
                    }
                    else
                    {
                        foreach (var item in lst)
                        {
                            ve(item);
                        }
                    }
                }
                else
                {
                    Recursionet(xeleClass.Elements(), xml_ClassNames, ref n, ve);
                }
                break;
            }
        }
    }
#endif
}
