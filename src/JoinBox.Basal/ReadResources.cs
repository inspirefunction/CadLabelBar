﻿namespace JoinBox.Basal;


using System.IO;
using System.Reflection;


public static class Resources
{
    /// <summary>
    /// 读取exe资源文件获取图标
    /// </summary>
    /// <param name="suffix">文件类型,ico</param>
    /// <param name="vlxExePath">exe路径</param>
    /// <param name="releaseOf">是否释放资源</param>
    /// <returns></returns>
    public static string EmbeddedFile(string suffix, string vlxExePath, bool releaseOf)
    {
        suffix = suffix.ToLower();

        string ico = "";
        var assm = Assembly.LoadFrom(vlxExePath);
        foreach (var resName in assm.GetManifestResourceNames())
        {
            if (!(Path.GetExtension(resName).ToLower() == suffix))
                continue;

            var xmls = assm.GetManifestResourceStream(resName);// 文件流
            if (xmls is null)
                continue;
            // 读取字节
            byte[] bts = new byte[xmls.Length];
            int a = -1;

            for (int i = 0; i < xmls.Length; i++)
                if ((a = xmls.ReadByte()) != -1)
                    bts[i] = (byte)a;

            ico = Path.GetFullPath(resName);
            if (releaseOf)
            {
                // 新建文件造一个
                using var sm = new FileStream(ico, FileMode.Create);
                sm.Write(bts, 0, bts.Length);
                sm.Close();
            }
        }
        return ico;
    }

}
