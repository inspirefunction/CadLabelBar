﻿namespace JoinBox.Basal;

using System;

public static class CreateClass
{
    #region IOC容器技术_通过形参生成
    /// <summary>
    /// 获取对象
    /// </summary>
    /// <typeparam name="T">类型</typeparam>
    /// <returns></returns>
    public static T? GetService<T>()
    {
        var a = GetService(typeof(T));
        if (a is not null)
            return (T)a;
        return default;
    }

    /// <summary>
    /// 获取对象
    /// </summary>
    /// <param name="type">类型</param>
    /// <returns></returns>
    public static object? GetService(Type type)
    {
        if (type is null)
            throw new ArgumentNullException(nameof(GetService));
        // 递归创建对象和属性赋值
        return Activator.CreateInstance(type);
    }
    #endregion
}