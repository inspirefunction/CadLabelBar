﻿namespace JoinBox.Basal;


using System.Text.RegularExpressions;


public static class RegexHelper
{
    // https://www.cnblogs.com/lonelyxmas/p/12437385.html
    /// <summary>
    /// 正整数
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    public static bool IsInt(string str)
    {
        return Regex.IsMatch(str, "[^0-9.-]+");
    }
    /// <summary>
    /// 是浮点数
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    public static bool IsDouble(string str)
    {
        /*匹配只能输入一个小数点的浮点数
          ^[.][0-9]+$ 从头到尾 小数点 数字出现 +是一个或多个
          | 或运算
          ^[0-9]*[.]{0,1}[0-9]*$  从头到尾
           [0-9]*   数字0个或多个
           [.]{0,1} 小数点出现0次或1次
           [0-9]*   数字0个或多个
           所以如果是一个小数点,那么这句判断成功,因为两边数字允许0次,而小数点允许0次和1次
        */
        return Regex.IsMatch(str, @"^[.][0-9]+$|^[0-9]*[.]{0,1}[0-9]*$");
    }
}
