# CadLabelBar
LabelBar Extension for AutoCad

Cad文档栏
 

## 项目说明:

CadLabelBar是在Acad2008上面利用WPF技术实现文档栏的功能,它是完全免费的.

因为在高版本cad上面已经有文档栏,但是Acad2008由于出生太早了,虽然有一个外国的文档栏插件,但是在win8期间出现各种bug,导致我产生了自制一个的想法,加上我未来可能基于文档栏的前提下发布其他插件,所以**惊惊**和**kingcai001**两人一起创立了此工程.

我们致力于适配上AutoDeskCAD/浩辰CAD/中望CAD等主流CAD.



## 工程说明:

1. main分支是WPF工程的分支,利用它你可以知道WPF的工程样例.

2. WinForm分支是先前制作的一个测试样例,它是能够运行的,但是它已经沉睡...若你仍然想开发这个分支我们欢迎你加入.




## 插件加载及使用方法： 

1. Win7~Win10平台(推荐使用Win10)

2. 程序支持AutoCad2008-2021.
   
3. 你需要在cad中使用netload命令加载此文件: src\LabelBar\bin\Debug\\**net35**\LabelBar.dll
   其中**net35**替换为你的cad需要的文件版本要求. 

       "net20" Acad2006 ;不支持
       "net20" Acad2007 ;不支持,没有com
       "net35" Acad2008
       "net35" Acad2009
       "net35" Acad2010
       "net35" Acad2011
       "net35" Acad2012
       "net40" Acad2013
       "net40" Acad2014
       "net45" Acad2015
       "net45" Acad2016
       "net46" Acad2017
       "net46" Acad2018
       "net47" Acad2019
       "net47" Acad2020
       "net48" Acad2021
   
4. 由于在net3.5上面开发和测试,Acad2008的用户可能需要在控制面板安装net3.5


## 技术文章： 

[WPF嵌入技术1:嵌入WPF到cad(MFC,win32窗体),Win32API嵌入WPF位置跳走的解决方案](https://www.cnblogs.com/JJBox/p/14078579.html)

[WPF嵌入技术2:将WPF嵌入到Acad2008的时候vs调试期间能成功,但是直接运行cad就不成功了(已解决)](https://www.cnblogs.com/JJBox/p/14125493.html)

[WPF嵌入技术3:Acad多线程的解决](https://www.cnblogs.com/JJBox/p/14179921.html)

[WPF嵌入技术4:重置cad之后创建文档出错,拦截cad致命错误](https://www.cnblogs.com/JJBox/p/14187031.html)


## 规范

### 编译和代码规范

[编译 IFox 源码工程](https://gitee.com/inspirefunction/ifoxcad/blob/jing/README.md)

[IFox 工程规范](https://gitee.com/inspirefunction/ifoxcad/blob/jing/docs/0x01%E4%BB%A3%E7%A0%81%E8%A7%84%E8%8C%83.md)

### 测试工程

测试CAD工程直接使用 LabelBar 工程.

测试WPF工程是脱离CAD的,需要独立一个WPF工程.

测试要保留测试参数的...可以给其他人看你写的参数对不对.

测试的WPF工程需要直接引用链接`文件/文件夹`,保证修改一致性,利用`CAD`(大小写敏感)预定义标签分离CAD部分.
若您需要增加测试工程,则也需要满足此条件
增加xaml的时候需要特别注意 `Generator` 标签
```xml
    <ItemGroup>
        <Compile Include="..\..\LabelBar\ViewModel\DPI.cs" Link="ViewModel\%(FileName)%(Extension)" />
        <Compile Include="..\..\LabelBar\ViewModel\Data.cs" Link="ViewModel\%(FileName)%(Extension)" />
        <Compile Include="..\..\LabelBar\View\CloseControl.xaml.cs" Link="View\%(FileName)%(Extension)" />
        <Compile Include="..\..\LabelBar\View\DocWindow.xaml.cs" Link="View\%(FileName)%(Extension)" />
    </ItemGroup>

    <ItemGroup>
        <Page Include="..\..\LabelBar\View\CloseControl.xaml" Link="View\%(FileName)%(Extension)">
            <!--有这行才可以在测试WPF的时候产生 InitializeComponent(); -->
            <Generator>MSBuild:Compile</Generator>
        </Page>
        <Page Include="..\..\LabelBar\View\DocWindow.xaml" Link="View\%(FileName)%(Extension)">
            <Generator>MSBuild:Compile</Generator>
        </Page>
    </ItemGroup>
```

## 作者信息:

本工程第一作者为:**惊惊** + **kingcai001**

技术顾问:**福萝卜**